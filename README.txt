Ensemble de script pour la petite reine blanche
dev: Guillaume Geelen


LPRB_TVPAINT_IMPORT_1.0.3.jsx
script AfterEffect exécutable via File > Script > Run script

LPRB_AE_customUI.jsx
script AfterEffect permettant d'exposer le script ci-dessus dans une petite interface avec ses petits boutons
(à copier ici: C:\Program Files\Adobe\Adobe After Effects 2024\Support Files\Scripts\ScriptUI Panels)

LPRB_Exporter.tvpx
Custom panel a utiliser dans TVPaint pour exporter vers AfterEffect
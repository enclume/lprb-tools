var dialog = createDockableUI(this);
    dialog.text = "Tools"; 
    dialog.orientation = "column"; 
    dialog.alignChildren = ["center","top"]; 
    dialog.spacing = 10; 
    dialog.margins = 16; 


var button0 = dialog.add("button", undefined, undefined, {name: "button0"}); 
    button0.text = "Conformation";
    button0.onClick  = function(){
        Confo();        
    }


var button1 = dialog.add("button", undefined, undefined, {name: "button1"}); 
    button1.text = "Send to Watchfolder";
    button1.onClick  = function(){
        if(app.project.file){

            app.project.save();
        
            var fileName = app.project.file.name
            var shortName = fileName.split('.')[0];
            var target = new File(["S:/06_COMPOSITING/08_WATCHFOLDER/" + fileName])    
            var thisFile = new File (app.project.file.fsName); // (of fsname - look it up)
            
            thisFile.copy(target);
        
            rcf_file = new File(["S:/06_COMPOSITING/08_WATCHFOLDER/" + shortName + "_RCF.txt"])
            rcf_file.open("w")
            rcf_file.writeln("After Effects 13.2v1 Render Control File")
            rcf_file.writeln("max_machines=1")
            rcf_file.writeln("num_machines=0")
            rcf_file.writeln("init=0")
            rcf_file.writeln("html_init=0")
            rcf_file.writeln("html_name=\"\"")
            rcf_file.close()
        
        
            alert(shortName + " send to watchfolder")
            
            }
    }

var button2 = dialog.add("button", undefined, undefined, {name: "button2"}); 
    button2.text = "Render Queue";
    button2.onClick  = function(){
        // var status_file = "S:/06_COMPOSITING/08_WATCHFOLDER/status.exe";

        // if (status_file.exists) {
        //     system.callSystem("S:/06_COMPOSITING/08_WATCHFOLDER/status.exe");

        // } else {
        //     alert("The WatchFolder progress file doesn't exist !");
        // }
        var f = File('S:/06_COMPOSITING/08_WATCHFOLDER/status.exe');
        f.execute();

    }

var button3 = dialog.add("button", undefined, undefined, {name: "button3"}); 
    button3.text = "Render Check";
    button3.onClick  = function(){
        // var status_file = "S:/06_COMPOSITING/08_WATCHFOLDER/status.exe";

        // if (status_file.exists) {
        //     system.callSystem("S:/06_COMPOSITING/08_WATCHFOLDER/status.exe");

        // } else {
        //     alert("The WatchFolder progress file doesn't exist !");
        // }
        var f = File('S:/06_COMPOSITING/08_WATCHFOLDER/Checker.exe');
        f.execute();

    }
    
showWindow(dialog);

function Confo() {    
    var template_path = "S:/06_COMPOSITING/00_DIVERS/SIR_COMPO_SQ##PL###_V##.aep";
    var export_comp_name = "SIR_COMPO_SQ##PL###";
    var animatique_prefix = "SIR_S-T-B_";
    var animatique_maj_suffix = "_maj.mov";
    var animatique_mp4_suffix = ".mp4";

    var root_folder =  app.project.rootFolder;
    var template_file = new File(template_path);
    var master_comp = null;
    var base_comp = null;

    //EXPORT PATHS
    var export_seq_name = getAEPMasterName(app.project.file.name);
    var seq_pl_label =  export_seq_name.split('_')[2];
    var seq_label = seq_pl_label.substr(0,4);
    var export_version = parseInt(getAEPVersion(app.project.file.name),10);
    var export_version_label = "V" + ((export_version<10) ? ("0"+ export_version) : export_version);
    var export_full_name = export_seq_name+"_"+export_version_label;


    //COMP DE BASE
    for (var i = 1 ; i <= root_folder.numItems; i++) {
        var item = root_folder.items[i];
        if (item instanceof CompItem) {
            base_comp = item;
            break;
        }    
    }
    
    //CHECK BURN
    var to_burn = true
    if (base_comp.width == 2048 && base_comp.height == 958) {
        alert("Conformation done");
        to_burn = false
    }

    //BURN
    if (to_burn){
        if (base_comp === null) {
            alert("The comp must be in the root folder of the project");
            return
        }


        //MOVE TO FOLDER
        var comp_folder = app.project.items.addFolder(getAEPMasterName(app.project.file.name));

        for (var i = 1 ; i <= root_folder.numItems; i++) {
            var item = root_folder.items[i];
            if (item != comp_folder){
                item.parentFolder = comp_folder;
            }
        }

        //IMPORTATION
        var import_options = new ImportOptions();
        var import_folder = template_file.parent;
        import_options.file = template_file;
        var import_item = app.project.importFile(import_options);

        //COMP TEMPLATE
        for (var i = 1 ; i <= import_item.numItems; i++) {
            var item = import_item.items[i];
            if (item instanceof CompItem) {
                master_comp = item;
                break;
            }
        }

        // //SCALE COMP
        // if (base_comp.width == 2560 && base_comp.height == 1073) {
        //     var widthOld = base_comp.width;
        //     var widthNew = 2815;
        //     var widthDelta = widthNew - widthOld;

        //     var heightOld = base_comp.height;
        //     var heightNew = 1327;
        //     var heightDelta = heightNew - heightOld;

        //     base_comp.width = widthNew;
        //     base_comp.height = heightNew;

        //     var null3DLayer = base_comp.layers.addNull();
        //     null3DLayer.threeDLayer = true;
                    
        //     // Set its position to (0,0,0).
        //     null3DLayer.position.setValue([0,0,0]);
            
        //     // Set null3DLayer as parent of all layers that don't have parents.  
        //     makeParentLayerOfAllUnparented(base_comp, null3DLayer);
        //     null3DLayer.position.setValue([widthDelta/2,heightDelta/2,0]);
        //     null3DLayer.remove();
            
        // } else {
        //     alert("Check base comp resolution")
        // }

        //IMPORT BASE COMP IN MASTER COMP
        master_comp.duration = base_comp.duration;

        var base_comp_layer = master_comp.layers.add(base_comp);
        base_comp_layer.transform.scale.setValue([80,80]);
        base_comp_layer.moveToEnd();





        // IMPORT ANIMATIQUE FOR SOUND
        var animatique_folder_path = app.project.file.parent.parent.path+"/09_ANIMATIQUE/"+seq_label+"/";
        var animatique_file_common_path = animatique_folder_path+animatique_prefix+seq_pl_label;
        var animatique_file_path = animatique_file_common_path+animatique_maj_suffix;
        var animatique_file = new File(animatique_file_path);
        if (!animatique_file.exists) {
            $.writeln("maj mov animatique does not existe : "+animatique_file_path);
            animatique_file_path = animatique_file_common_path+animatique_mp4_suffix;
            animatique_file = new File(animatique_file_path);
            if (!animatique_file.exists) {
                $.writeln("mp4 animatique does not existe : "+animatique_file_path);
            }
        }

        if (animatique_file.exists) {
            var import_animatique_options = new ImportOptions();
            import_animatique_options.file = animatique_file;
            var animatique_item = app.project.importFile(import_animatique_options);
            animatique_item.parentFolder = comp_folder
            var animatique_layer = master_comp.layers.add(animatique_item);
            animatique_layer.moveToEnd();
            
            base_comp_layer.enabled = true;
            base_comp_layer.audioEnabled = false;
            
            animatique_layer.enabled = false;
            animatique_layer.audioEnabled = true;
            
            
        } else {
            base_comp_layer.enabled = true;
            base_comp_layer.audioEnabled = true;
        }

        //FINAL ORGANISATION
        master_comp.name = export_full_name;
        //base_comp.name += "_COMP";
        base_comp.parentFolder = comp_folder;
        master_comp.parentFolder = app.project.rootFolder;
        app.project.removeUnusedFootage()
        master_comp.openInViewer()
    } //END BURN

    //CHECK COMPO NAME, LOCK AND FX
    for (var i = 1 ; i <= root_folder.numItems; i++) {
        var item = root_folder.items[i];
        if (item instanceof CompItem) {
            item.name = export_full_name;
            for (var j = 1; j <= item.numLayers; j++) {
                var layer = item.layer(j);
                layer.locked = true;
                //BASE COMP PROCESS
                if (layer.source instanceof CompItem) {
                    layer.locked = false;                    
                    var effects = layer.property("Effects")    
                    //REMOVE ALL EFFECTS                
                    for (h = effects.numProperties; h > 0; h--){
                        effects.property(h).remove();
                      }
                    //ADD CC REPETILE
                    var tileH = layer.Effects.addProperty("CC RepeTile");
                    tileH.property("Expand Right").setValue(10);
                    tileH.property("Expand Left").setValue(10);
                    tileH.property("Tiling").setValue(2);
                    tileH.name = "BorderH"

                    var tileV = layer.Effects.addProperty("CC RepeTile");
                    tileV.property("Expand Down").setValue(10);
                    tileV.property("Expand Up").setValue(10);
                    tileV.property("Tiling").setValue(3);
                    tileV.name = "BorderV"                    
                }
            }
            break
        }    
    }

    //CHECK CADRAGE
    for (var i = 1 ; i <= app.project.items.length; i++) {
        var item = app.project.items[i];
        if (item instanceof CompItem) {
            processBGComp(item)
        }    
    }

    //RENDER CONFIG
    for (var i = 1 ; i <= root_folder.numItems; i++) {
        var item = root_folder.items[i];
        if (item instanceof CompItem) {
            master_comp = item;
            break;
        }    
    }    
    var export_folder_path = app.project.file.parent.parent.path+"/06_FROM_ENCLUME/EXPORT/"+seq_label+"/";
    setRenderItemConfig(export_folder_path,export_full_name,seq_pl_label, master_comp);

    //SCREENSHOT
    var screenshots_folder_path = export_folder_path+"_screenshots";
    var f = new Folder(screenshots_folder_path);
    if (!f.exists) f.create();
    var screenshot_path = screenshots_folder_path+"/"+export_full_name+".png";
    var screenshot_file = File(screenshot_path);
    master_comp.saveFrameToPng(1, screenshot_file);
    var t = 0;
    while (!screenshot_file.exists) {
        t++
        $.sleep(500);
    }
} // END CONFO

//FUNCTIONS
function getAEPVersion(name) {
    var pattern = /_V(\d+)/;
    var res = name.match(pattern);
    if (res && res.length > 1) return res[1];
    return 0;
}

function getAEPMasterName(name) {
    var pattern = /((.+)(?:_V\d+).aep)/;
    var res = name.match(pattern);
    if (res && res.length > 1) return res[2];
    return "";
}

function endWithLog(message) {
    $.writeln("END WITH LOG : "+message);
}

function setRenderItemConfig(export_folder_path, sq_pl_version, sp_pl, master_comp)
{  
    //CREATE FOLDERS
    var f = new Folder(export_folder_path);
    if (!f.exists) f.create();    
    var default_path = export_folder_path+"/"+sq_pl_version;
    var tiff_folder_path = default_path;
    var tiff_path = tiff_folder_path+"/SIR_COMPO_"+sp_pl+"_[#####]"
    var mp4_path = default_path;
    var dnx_path = default_path;    
    var f = new Folder(export_folder_path);
    if (!f.exists) f.create();
    var f = new Folder(tiff_folder_path);
    if (!f.exists) f.create();
    
    //cleanRenderQueue
    while (app.project.renderQueue.numItems > 0) {
        app.project.renderQueue.item(1).remove();
   
   }
    
    // master_comp
    var renderItem = app.project.renderQueue.items.add(master_comp);
    
    renderItem.applyTemplate("SIR_FULL");

    var om1 = renderItem.outputModule(1);
    om1.applyTemplate("SIR_TIFF");  
    om1.file = File(tiff_path);

    var om2 = renderItem.outputModules.add();    
    om2.applyTemplate("SIR_DNX8BIT");
    om2.file = File(dnx_path);

    var om2 = renderItem.outputModules.add();    
    om2.applyTemplate("SIR_MP4");
    om2.file = File(mp4_path);

    renderItem.render = true;
    
}

function createDockableUI(thisObj) {
    var dialog =
        thisObj instanceof Panel
            ? thisObj
            : new Window("window", undefined, undefined, { resizeable: true });
    dialog.onResizing = dialog.onResize = function() {
        this.layout.resize();
    };
    return dialog;
}

function showWindow(myWindow) {
    if (myWindow instanceof Window) {
        myWindow.center();
        myWindow.show();
    }
    if (myWindow instanceof Panel) {
        myWindow.layout.layout(true);
        myWindow.layout.resize();
    }
}

function makeParentLayerOfAllUnparented(theComp, newParent)
{
    for (var i = 1; i <= theComp.numLayers; i++) {
        var curLayer = theComp.layer(i);
        var wasLocked = curLayer.locked;
        curLayer.locked = false;
        if (curLayer != newParent && curLayer.parent == null) {
            curLayer.parent = newParent;
        }
        curLayer.locked = wasLocked
    }
}

function processBGComp(comp) {
    var n_layers = comp.numLayers;
   for (var i = 1; i <= n_layers; i++) {
       layer = comp.layer(i);
       if (layer.name.toLowerCase() == "cadrage") {
           layer.guideLayer = true;
       }
       if (layer.name.toLowerCase() == "cadre") {
        layer.guideLayer = true;
    }
   }
}
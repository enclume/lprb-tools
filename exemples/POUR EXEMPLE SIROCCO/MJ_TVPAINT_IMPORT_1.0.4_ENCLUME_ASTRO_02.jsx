/*
--------------------------------------------------------------
MJ_TVPAINT_IMPORT_1.0.3_ENCLUME_1.0.0
--------------------------------------------------------------
✓ setup du projet avec la colo 16bit et worskpace en Adobe RGB (1998)
✓  nouvelle arbo avec les sous dossiers et les comp/assets bien rangés
✓  les couleurs des layers correspondant aux règles que vous vouliez
✓  le png initial supprimé
--------------------------------------------------------------
MJ_TVPAINT_IMPORT_1.0.3_ENCLUME_1.0.1
--------------------------------------------------------------
✓ l'import des BG
✓ ajuster la durée de la comp BG et de ses layers
✓ application des presets
✓ import du mp4 ref en premier layer en mode "guide" et opacité 50%
✓ enregistrement du projet after dans le dossier 07_COMPO/SQXX/
--------------------------------------------------------------
TODO
× remettre les layers dans l'ordre des noms (bizarrement ça semble déjà dans le bon ordre !)

doc here : https://ae-scripting.docsforadobe.dev/layers/avlayer.html
Pour les chemins de sortie, c'est ici : https://ae-scripting.docsforadobe.dev/renderqueue/outputmodule.html
*/


//ENCLUME GLOBALS 
enclume_preset_line_path = "C:/Users/Marteau/Desktop/_N.ffx"
enclume_deco_folder_name = "02_DECORS_CONFO_TO_COMPO";
enclume_colo2compo_folder_name = "03_COLO_TO_COMPO";
enclume_compo_folder_name = "07_COMPO";
enclumeFolders = {};
enclume_preset_line = new File(enclume_preset_line_path);


if (isSecurityPrefSet() == true) {

    
    
    //ENCLUME PREPROCESS
    enclume_preprocess();

    firstImportedPNG = app.project.activeItem;
    if(firstImportedPNG===null){
		alert("Please Select a PNG Exported From TVPaint 1" );
	} else {
            try {
            var lele = firstImportedPNG.typeName;
            if(lele=="Footage"){

                    enclumeCompositingRootFolder = firstImportedPNG.file.parent.parent.parent.parent.parent;
                    clipFolder = firstImportedPNG.file.parent.parent; 
                    clipFolderInfos = enclume_getProjectInfosFromName(clipFolder.name);                   
                    clipInfopath= clipFolder.absoluteURI+"/clipinfo.txt";
                    clipInfo = new File(clipInfopath);
                    
    
                    if (clipInfo.exists){
                        
                          clipInfo.open();
				
                        var framerate = clipInfo.readln();
                        framerate = framerate.split(";");
                        framerate = framerate[1];
            
                        var clipduration = clipInfo.readln();
                        clipduration = clipduration.split(";");
                        clipduration = clipduration[1];
						
                        var bgcolor = clipInfo.readln();
                        bgcolor = bgcolor.split(";");
                        bgcolor = bgcolor[1];
					    bgcolor = bgcolor.split(",");
					    bgcolor = new Array(bgcolor[0],bgcolor[1],bgcolor[2]);
                        clipInfo.close();
						
	
                        projClipFolder = app.project.items.addFolder(clipFolder.displayName);
                        //ENCLUME SUBFOLDERS
                        enclumeFolders.bg = enclume_createFolder("_BG",projClipFolder);
                        enclumeFolders.camera = enclume_createFolder("_camera",projClipFolder);
                        enclumeFolders.anim_line = enclume_createFolder("ANIM_LINE",projClipFolder);
                        enclumeFolders.colo = enclume_createFolder("COLO",projClipFolder);
                        enclumeFolders.colo_line = enclume_createFolder("COLO_LINE",projClipFolder);
                        //-------------------
                        myNewCompositions = new Array();
                        var folders = folders_get(clipFolder);
                        for(var i = 0; i < folders.length; i++){
                            var curLayerFolder = folders[i];
                            var folderName = curLayerFolder.displayName;
                            var itemArray = curLayerFolder.getFiles("*.png");
                            if(itemArray.length > 0){
                                itemArray.sort();
                            }
                            var folderNameInfo = enclume_getInfosFromName(folderName);
                            projLayerFolder = app.project.items.addFolder(folderName);
                            // projLayerFolder.parentFolder = projClipFolder;
                            projLayerFolder.parentFolder = folderNameInfo.folder;
                            	//Importing
                                var footage = new Array();
                                var files = new Array();
                                for(var j = 0; j < itemArray.length;j++){
                                    var curFile = new File(itemArray[j]);
                                    var ioRef = new ImportOptions(curFile);
                                    ioRef.importAs = ImportAsType.FOOTAGE;
                                    var footageRef = app.project.importFile(ioRef);
                                    footageRef.parentFolder = projLayerFolder;
                    
                                    footage[j] = footageRef;
                                  
                                }
                            
                                // read layerinfo
                                layerInfopath= curLayerFolder.absoluteURI+"/layerinfo.txt";
                                layerInfo = new File(layerInfopath);

                                if (layerInfo.exists){
                                   layerInfo.open();
				
                                        var layerIN = layerInfo.readln();
                                        layerIN = layerIN.split(";");
                                        layerIN = layerIN[1];
                                        
                                         var layerOUT = layerInfo.readln();
                                        layerOUT = layerOUT.split(";");
                                        layerOUT = layerOUT[1];
                                        
                                        var layerBlendmode = layerInfo.readln();
                                        layerBlendmode = layerBlendmode.split(";");
                                        layerBlendmode = layerBlendmode[1];
										
									var layerOpacity = layerInfo.readln();
                                        layerOpacity = layerOpacity.split(";");
                                        layerOpacity = layerOpacity[1];
									try{							
										var layerLabel = layerInfo.readln();
										layerLabel = layerLabel.split(";");
										layerLabel = layerLabel[1]*1;
									} catch(e){
										layerLabel = 0;
									}
                                    
                                 
                                    layerInfo.close();
                                } else {
                                     layerIN = 1
                                    layerOUT = layerIN+clipduration
                                    layerBlendmode ="COLOR";
                                 }
                             
                                layerDuration = layerOUT-layerIN+1
                                layerDuration = layerDuration/framerate
                              
                                //Creating Composition

                                curComp = app.project.items.addComp(folderName,footageRef.width,footageRef.height,1,layerDuration,framerate);
                                //curComp.parentFolder = projClipFolder;
                                curComp.parentFolder = folderNameInfo.folder;
                                var compStats=new Array(curComp,layerIN,layerOUT,layerBlendmode,layerOpacity,layerLabel);
                                myNewCompositions.push(compStats);
                                //Timing layers

                                for(k=0;k<footage.length;k++){
                                    curImage = footage[k];
                                    path = curImage.file.absoluteURI;
                                    imageNUM =  filepath_getNumber(path)*1;

                                    //frame duration
                                    var imageIN = imageNUM/framerate-layerIN/framerate;

                                    if(k==footage.length-1){
                                        var imageDuration =  curComp.duration-imageIN;
                                    }else{
                                        nextImage = footage[k+1];
                                        path = nextImage.file.absoluteURI;
                                        nextNUM =  filepath_getNumber(path)*1;
                                        var deltaFrame = nextNUM-imageNUM;
                                        var imageDuration = deltaFrame/framerate;
                                    }

                                    //finding file

                                    try{
                                        mylayers = curComp.layers.add(curImage);
                                        mylayers.inPoint = imageIN;
                                        mylayers.outPoint = imageIN+imageDuration;
                                    } catch(e){
                                        alert("Cant Add Frame "+xsht_file+"\nof "+nameMain+"\n Probably because the footage wasnt imported because there is a write failue in the File");
                                    }

                                }

                              


                            
                        }
                        
					//collect Layers In animation Comp

					var collectComp = app.project.items.addComp(clipFolder.displayName,myNewCompositions[0][0].width,myNewCompositions[0][0].height,1,clipduration/framerate,framerate);
					collectComp.parentFolder = projClipFolder;
				
				
					collectComp.bgColor = bgcolor;
					for(var p = myNewCompositions.length;p>0 ;p--){
						var curCompStats = myNewCompositions[p-1];
						var curComp=curCompStats[0];
						var curIN=curCompStats[1];
						var curOUT=curCompStats[2];
						var curBLEND=curCompStats[3];
						var curOPACITY=curCompStats[4];
						var curLABEL=curCompStats[5];
						var curLayer = collectComp.layers.add(curComp);
						layer_setBlendMode(curLayer,curBLEND);
						curLayer.startTime =(curIN-1)/framerate;
						curLayer.opacity.setValue(curOPACITY);
				
						// try{
						// 		curLayer.label=curLABEL*1;
						// } catch(e){
						// 		curLayer.label=15;
						// }

                        //ENCLUME OVERRIDES
                        var curCompNameInfo = enclume_getInfosFromName(curComp.name);
                        curLayer.label = curCompNameInfo.layerColor;
                        if (curCompNameInfo.type === "BG") curLayer.remove();
                        if (curCompNameInfo.type === "ANIM_LINE") {
                            curLayer.applyPreset(enclume_preset_line);
                            curLayer.blendingMode = BlendingMode.MULTIPLY;
                        }
					
					}
                    try {
                        enclume_postprocess();
                    } catch (e) {
                        alert("ENCLUME POST PROCESS : ("+e.line+") "+e.message);
                    }
                   
                            
                    } else {
                        alert("'clipinfo.txt' do not exists in clipfolder. Please re-export from TVPaint");
                    }

            } else {
                alert("MESSAGE 2 :Please Select a PNG Exported From TVPaint" );
            }
        } catch (e) {
            alert("MESSAGE 3 :Please Select a PNG Exported From TVPaint "+e.line+":"+e.message);
        }
    }

    


} else {
	alert ("This Script requires the scripting security preference to be set.\nGo to the \"General\" panel of your application preferences, and make sure that \"Allow Scripts to Write Files and Access Network\" is checked.", "MJ TVPAINT IMPORT");
}


function enclume_preprocess() {
    // alert("preprocess_enclume");
    app.project.bitsPerChannel = 16;
    app.project.workingSpace = "Adobe RGB (1998)";
}

function enclume_postprocess() {
    // alert("postprocess_enclume");

    var bg_comp = enclume_importBG();
    video_ref_path = enclumeCompositingRootFolder.fullName+"/"+enclume_colo2compo_folder_name+"/SQ"+clipFolderInfos.sq;
    video_ref_footage = enclume_importVideoRef(video_ref_path, collectComp, false);

    /*
     *  ASTRONAUTES SPEC:
     */
    var template_comp_tif = astro_get_comp_by_name("SIR_COMPO_SQ##PL###")
    template_comp_tif.duration = collectComp.duration;
    // template_comp_tif.layers.add(video_atk_layer)

    var template_comp = astro_get_comp_by_name("SIR_COMPO_SQ##PL###")
    template_comp.duration = collectComp.duration;

    video_atk_path = "//srvdata02/FABRICATION/SIROCCO/02_ANIMATIQUE/03_PLANS/SQ"+clipFolderInfos.sq;
    video_atk_footage = enclume_importVideoRef(video_atk_path, template_comp_tif);
    var video_atk_layer = astro_import_video_footage_in_comp(video_atk_footage, collectComp, 125, true, false)
    video_atk_layer.scale.setValue([125,125,125])
    // video_atk_layer.opacity = 50

    if ( (bg_comp != false) && (bg_comp.layers[1].name == "CADRAGE") ) 
	{
        var bg_cadre_src = bg_comp.layers[1].source
        var bg_cadre = collectComp.layers.add(bg_cadre_src)
        bg_cadre.moveToBeginning()
        bg_cadre.guideLayer = true
        bg_comp.layers[1].remove()
    }
    var comp_sq_pl = collectComp.name.split('_')[2]
    var tvpComp_in_tif_comp = template_comp_tif.layers.add(collectComp)
    tvpComp_in_tif_comp.moveToEnd()
    tvpComp_in_tif_comp.audioEnabled = false
    tvpComp_in_tif_comp.scale.setValue([80,80,80])
    template_comp_tif.name = template_comp_tif.name.replace("SQ##PL###", comp_sq_pl)
    template_comp.name = template_comp.name.replace("SQ##PL###", comp_sq_pl)

	// Change Render Path
	fctRenderingConfigurationPath(comp_sq_pl)

    /*
     *  Back to _enclume_ post process:
     */

    firstImportedPNG.remove();
    collectComp.openInViewer();
    //enclume_saveProject();

}

function fctRenderingConfigurationPath(currentShotName)
{
	sequenceName = currentShotName.substr(0,4);
	newCompName = currentShotName;
    newDnxName = currentShotName;
    newTiffName = currentShotName;
	
	dnxRenderFolderPath = "S//06_COMPOSITING//06_FROM_ENCLUME//EXPORT//"+sequenceName+"//EXPORTS_DNxHD//"+newDnxName;
	mp4RenderFolderPath = "S//06_COMPOSITING//06_FROM_ENCLUME//EXPORT//"+sequenceName+"//MP4//"+currentShotName;
	
	var f = new Folder("S//06_COMPOSITING//06_FROM_ENCLUME//EXPORT//"+sequenceName);
    if (!f.exists)
        f.create();
	
	var f = new Folder("S//06_COMPOSITING//06_FROM_ENCLUME//EXPORT//"+sequenceName+"//TIFF//"+newTiffName);
    if (!f.exists)
        f.create();
	tiffRenderFolderPath = "S//06_COMPOSITING//06_FROM_ENCLUME//EXPORT//"+sequenceName+"//TIFF//"+newTiffName+"//"+newTiffName+"_[#####]";
	
	var f = new Folder("S//06_COMPOSITING//06_FROM_ENCLUME//EXPORT//"+sequenceName+"//EXPORTS_DNxHD");
    if (!f.exists)
        f.create();
	
	var f = new Folder("S//06_COMPOSITING//06_FROM_ENCLUME//EXPORT//"+sequenceName+"//MP4");
    if (!f.exists)
        f.create();
	
    //redefine output path TIFF
	tiffItem = app.project.renderQueue.item(1);	
    //tiffItemTemplate = tiffItem.applyTemplate("SIR_FULL")
    //tiffOutputTemplate = tiffItem.outputModule(1).applyTemplate("SIR_TIFF");
	tiffOutputModule = tiffItem.outputModule(1);
	tiffOutputModule.file = File(tiffRenderFolderPath);
    
	dnxOutputModule = tiffItem.outputModule(2);
	dnxOutputModule.file = File(dnxRenderFolderPath);
	
	mp4OutputModule = tiffItem.outputModule(3);
	mp4OutputModule.file = File(mp4RenderFolderPath);
	
	tiffItem.render = true;
	
	// output path DNX
	//dnxItem = app.project.renderQueue.item(2);
    //dnxItemTemplate = mp4Item.applyTemplate("SIR_FULL")
    //dnxOutputTemplate = mp4Item.outputModule(1).applyTemplate("SIR_TIFF");
	//dnxOutputModule = dnxItem.outputModule(1);
	//dnxOutputModule.file = File(dnxRenderFolderPath);
	//dnxItem.render = true;
	
	// output path MP4
	//mp4Item = app.project.renderQueue.item(3);
    //mp4ItemTemplate = mp4Item.applyTemplate("SIR_FULL")
    //mp4OutputTemplate = mp4Item.outputModule(1).applyTemplate("SIR_TIFF");
	//mp4OutputModule = mp4Item.outputModule(1);
	//mp4OutputModule.file = File(mp4RenderFolderPath);
	//mp4Item.render = true;
}

function enclume_saveProject() {
    var saveSeqFolderPath = enclumeCompositingRootFolder.fullName+"/"+enclume_compo_folder_name+"/SQ"+clipFolderInfos.sq;
    var saveSeqFolder = new Folder(saveSeqFolderPath);
    if (!saveSeqFolder.exists) saveSeqFolder.create();
    var saveProjectPath = saveSeqFolderPath+"/"+clipFolder.displayName+".aep";
    app.project.save(new File(saveProjectPath));
}

function enclume_importVideoRef(videoRefFolderPath, destination_comp, visible) {
    // enlume_log(enclumeCompositingRootFolder.fullName);
    //videoRefFolderPath = enclumeCompositingRootFolder.fullName+"/"+enclume_colo2compo_folder_name+"/SQ"+clipFolderInfos.sq;
    videoRefFolder = new Folder(videoRefFolderPath);
    videoRefFiles = videoRefFolder.getFiles("*.mp4");
    videoRefFile = null;
    for (var i=0;i<videoRefFiles.length;i++) {
        var file = videoRefFiles[i];
        if (file.name.lastIndexOf(clipFolderInfos.sqpl) != -1) {
            videoRefFile = file;
            break;
        }
    }
    //seqDecoPath = seqDecoFolderPath+"/SIR_CONFO_"+clipFolderInfos.sqpl+"_V01.psd";
    //seqDecoFile = new File(seqDecoPath);
    if (!videoRefFile) {
        alert("Couldn't find the ref .mp4 file for "+clipFolderInfos.sqpl);
        return;
    }

    videoRefImportOptions = new ImportOptions(videoRefFile);
    // bgImportOptions.importAs = ImportAsType.COMP;
    videoRefFootage = app.project.importFile(videoRefImportOptions);
    videoRefFootage.parentFolder = projClipFolder;

    astro_import_video_footage_in_comp(videoRefFootage, destination_comp, 100, visible, true)
    
    // videoRefLayer = destination_comp.layers.add(videoRefFootage);
    // videoRefLayer.moveToBeginning();
    // videoRefLayer.guideLayer = true;
    // videoRefLayer.opacity.setValue(50);
    // videoRefLayer.label = 0;

    return videoRefFootage
}

function astro_import_video_footage_in_comp(videoRefFootage, comp, scale, visible, audible) {
    videoRefLayer = comp.layers.add(videoRefFootage);
    videoRefLayer.moveToBeginning();
    videoRefLayer.guideLayer = true;
	
	var videoRefLayerName = videoRefLayer.name;
	
	videoRefLayer.audioEnabled = audible;
	
	if (videoRefLayerName.indexOf("SIR_COLOR_") == 0)
		videoRefLayer.audioEnabled = false;
	
    videoRefLayer.opacity.setValue(50);
    videoRefLayer.label = 0;
    videoRefLayer.visible = visible;
    if (scale) 
	{
        videoRefLayer.scale.setValue([scale,scale,scale])
    }
    return videoRefLayer
}


function enclume_importBG() {
    
    // enlume_log(enclumeCompositingRootFolder.fullName);
    decoRootFolderPath = enclumeCompositingRootFolder.fullName+"/"+enclume_deco_folder_name;
    seqDecoFolderPath = decoRootFolderPath+"/SQ"+clipFolderInfos.sq;
    seqDecoFolder = new Folder(seqDecoFolderPath);
    seqDecoFiles = seqDecoFolder.getFiles("*.psd");
    seqDecoFile = null;
    for (var i=0;i<seqDecoFiles.length;i++) {
        var file = seqDecoFiles[i];
        if (file.name.lastIndexOf(clipFolderInfos.sqpl) != -1) {
            seqDecoFile = file;
            break;
        }
    }
    //seqDecoPath = seqDecoFolderPath+"/SIR_CONFO_"+clipFolderInfos.sqpl+"_V01.psd";
    //seqDecoFile = new File(seqDecoPath);
    if (!seqDecoFile) {
        alert("Couldn't find the BG .psd file for "+clipFolderInfos.sqpl);
        return false;
    }

    bgImportOptions = new ImportOptions(seqDecoFile);
    bgImportOptions.importAs = ImportAsType.COMP;
    bgComp = app.project.importFile(bgImportOptions);
    bgComp.duration = collectComp.duration;
    bgComp.frameRate = collectComp.frameRate;
    bgComp.frameDuration = collectComp.frameDuration;

    for (var i = 1; i <= bgComp.numLayers; i++) {
        var l = bgComp.layers[i]
        l.outPoint = bgComp.duration;
    }

    var bgCompName = bgComp.name
    var bgCompNewName = bgCompName.replace("SIR_CONFO_","BG_")

    bgComp.parentFolder = enclumeFolders.bg;
    bgComp.name = bgCompNewName;

    for (var i = 1; i <= app.project.numItems; i++) {
        var item = app.project.item(i)
        if (item.name === bgCompName+" Layers") {
            item.parentFolder = enclumeFolders.bg;
            item.name = bgCompNewName;
        }
    }

    bgLayer = collectComp.layers.add(bgComp);
    bgLayer.label = 0;
    bgLayer.moveToEnd();

    return bgComp
}

function enlume_log(message) {
    $.writeln(message);
    alert(message);
}

function enclume_createFolder(name,parentFolder) {
    var folder = app.project.items.addFolder(name);
    folder.parentFolder = parentFolder;
    return folder;
}
function enclume_getProjectInfosFromName(name) {
    var pattern = /(SQ(\d+)PL(\d+))/;
    var res = name.match(pattern);
    var sqpl = res[1];
    var sq = res[2];
    var pl = res[3];
    return {
        sqpl : sqpl,
        sq : sq,
        pl : pl
    }
}

function enclume_getInfosFromName(name) {
   
   var infos = {
        type : "COLO",
        content : null,
        perso : null,
        folder : enclumeFolders.colo,
        layerColor : 0
   }

   //CONTENT
   var content;
   if (name.lastIndexOf("CAR") != -1) content = "CAR";
   if (name.lastIndexOf("SEL") != -1) content = "SEL";
   if (name.lastIndexOf("MAI") != -1) content = "MAI";
   if (name.lastIndexOf("CRO") != -1) content = "CRO";
   if (name.lastIndexOf("JUL") != -1) content = "JUL";
   if (name.lastIndexOf("JOU") != -1) content = "JOU";
   if (name.lastIndexOf("FIL") != -1) content = "FIL";
   if (name.lastIndexOf("SIR") != -1) content = "SIR";
   if (name.lastIndexOf("BG") != -1) content = "BG";
   infos.content = content;
   //TYPE
   var last2chars = name.substr(-2);
   var last3chars = name.substr(-3);
   if (last2chars === "_N") infos.type = "ANIM_LINE";
   if (last2chars === "_R") infos.type = "COLO_LINE";
   if (last3chars === "_BG") infos.type = "BG";
   //FOLDER
    switch (infos.type) {
        case "ANIM_LINE" : infos.folder = enclumeFolders.anim_line; break;
        case "COLO_LINE" : infos.folder = enclumeFolders.colo_line; break;
        case "BG" : infos.folder = enclumeFolders.bg; break;
    }
   //LAYER COLOR
   switch(infos.type) {
    case "ANIM_LINE" :
        switch (content) {
            case "CAR" : infos.layerColor = 16; break; //DARK GREEN
			case "SEL" : infos.layerColor = 10; break; //PURPLE
			case "MAI" : infos.layerColor = 12; break; //BROWN
            case "JUL" : infos.layerColor = 1; break; //RED
            case "JOU" : infos.layerColor = 11; break; //ORANGE
			case "CRO" : infos.layerColor = 16; break; //DARK GREEN
			case "FIL" : infos.layerColor = 15; break; //SANDSTONE
			case "SIR" : infos.layerColor = 8; break; //BLUE
        }
        break;
    case "BG" :
        infos.layerColor = 0; break; //GREY
        break;
    default :
        switch (content) {
            case "CAR" : infos.layerColor = 9; break; //GREEN
			case "SEL" : infos.layerColor = 5; break; //LAVENDER
			case "MAI" : infos.layerColor = 11; break; //ORANGE
            case "JUL" : infos.layerColor = 4; break; //PINK
            case "JOU" : infos.layerColor = 2; break; //YELLOW
            case "BG" : infos.layerColor = 0; break; //GREY
			case "CRO" : infos.layerColor = 3; break; //AQUA
			case "FIL" : infos.layerColor = 6; break; //PEACH
			case "SIR" : infos.layerColor = 14; break; //CYAN
        }
        break;
    }

    return infos;

}



function astro_get_comp_by_name(comp_name) {
    for (var i = 1; i <= app.project.numItems; i++) {
        var item = app.project.item(i)
        if (item instanceof CompItem && item.name == comp_name ) {
            return item
        }
    }  
}

function folders_get(theFolder){
	try{
		//var returnFolders = new Array();
		//var j = 0;
		
		var folderItems = theFolder.getFiles(folders_find);
		return folderItems;
	}catch(e){
		alert(e.line+":"+e.message);
	}
}


function folders_find(theFolder){
	try{
		if(theFolder instanceof Folder)
		{
			return true;
		}else{
			return false;
		}
	}catch(e){
		alert(e.line+":"+e.message);
	}
}

function filepath_getNumber(path){
    var string_noExt = path.substring(0,path.length-4);
	var numberi = 0;
	var namei = 0;
	for(var i = string_noExt.length-1;i>=0;i--){
		if(isNaN(parseInt(string_noExt.charAt(i)))){
			numberi = i+1;
			break;
		}
		
	}
    number = string_noExt.substring(numberi,string_noExt.length);
    return number;
}


function layer_setBlendMode(curLayer,curBLEND){
						switch (curBLEND)
						{
						case "Color":
							curLayer.blendingMode = BlendingMode.NORMAL;
						  break;
						case "Add":
						 curLayer.blendingMode =  BlendingMode.ADD;
						case "Screen":
						  curLayer.blendingMode =  BlendingMode.SCREEN;
						  break;
						case "Multiply":
						    curLayer.blendingMode =  BlendingMode.MULTIPLY;
						  break;
						case "Overlay":
						  curLayer.blendingMode =  BlendingMode.OVERLAY;
						  break;
		
						  default:
						  curLayer.blendingMode = BlendingMode.NORMAL;
						}
						
						
}

function isSecurityPrefSet(){
	var securitySetting = app.preferences.getPrefAsLong("Main Pref Section", "Pref_SCRIPTING_FILE_NETWORK_SECURITY");
	return (securitySetting == 1);
}

		

		



﻿if (isSecurityPrefSet() == true) {
    var myItem = app.project.activeItem;
    if(myItem===null){
		alert("Please Select a PNG Exported From TVPaint 1" );
	} else {
            try {
            var lele = myItem.typeName;
            if(lele=="Footage"){
           
                    clipFolder = myItem.file.parent.parent;
                    clipInfopath= clipFolder.absoluteURI+"/clipinfo.txt";
                    clipInfo = new File(clipInfopath);
    
                    if (clipInfo.exists){
                        
                          clipInfo.open();
				
                        var framerate = clipInfo.readln();
                        framerate = framerate.split(";");
                        framerate = framerate[1];
            
                        var clipduration = clipInfo.readln();
                        clipduration = clipduration.split(";");
                        clipduration = clipduration[1];
						
                        var bgcolor = clipInfo.readln();
                        bgcolor = bgcolor.split(";");
                        bgcolor = bgcolor[1];
					  bgcolor = bgcolor.split(",");
					  bgcolor = new Array(bgcolor[0],bgcolor[1],bgcolor[2]);
                        clipInfo.close();
						
	
                        projClipFolder = app.project.items.addFolder(clipFolder.displayName);
                        myNewCompositions = new Array();
                        var folders = folders_get(clipFolder);
                        for(var i = 0; i < folders.length; i++){
                            var curLayerFolder = folders[i];
                            var folderName = curLayerFolder.displayName;
                            var itemArray = curLayerFolder.getFiles("*.png");
                            if(itemArray.length > 0){
                                itemArray.sort();
                            }
                            projLayerFolder = app.project.items.addFolder(folderName);
                            projLayerFolder.parentFolder = projClipFolder;
                            	//Importing
                                var footage = new Array();
                                var files = new Array();
                                for(var j = 0; j < itemArray.length;j++){
                                    var curFile = new File(itemArray[j]);
                                    var ioRef = new ImportOptions(curFile);
                                    ioRef.importAs = ImportAsType.FOOTAGE;
                                    var footageRef = app.project.importFile(ioRef);
                                    footageRef.parentFolder = projLayerFolder;
                    
                                    footage[j] = footageRef;
                                  
                    
                                }
                            
                                // read layerinfo
                                layerInfopath= curLayerFolder.absoluteURI+"/layerinfo.txt";
                                layerInfo = new File(layerInfopath);

                                if (layerInfo.exists){
                                   layerInfo.open();
				
                                        var layerIN = layerInfo.readln();
                                        layerIN = layerIN.split(";");
                                        layerIN = layerIN[1];
                                        
                                         var layerOUT = layerInfo.readln();
                                        layerOUT = layerOUT.split(";");
                                        layerOUT = layerOUT[1];
                                        
                                        var layerBlendmode = layerInfo.readln();
                                        layerBlendmode = layerBlendmode.split(";");
                                        layerBlendmode = layerBlendmode[1];
										
									var layerOpacity = layerInfo.readln();
                                        layerOpacity = layerOpacity.split(";");
                                        layerOpacity = layerOpacity[1];
									try{							
										var layerLabel = layerInfo.readln();
										layerLabel = layerLabel.split(";");
										layerLabel = layerLabel[1]*1;
									} catch(e){
										layerLabel = 0;
									}
                                    
                                 
                                    layerInfo.close();
                                } else {
                                     layerIN = 1
                                    layerOUT = layerIN+clipduration
                                    layerBlendmode ="COLOR";
                                 }
                             
                                layerDuration = layerOUT-layerIN+1
                                layerDuration=layerDuration*(1/framerate)
                              
                                //Creating Composition

                                curComp = app.project.items.addComp(folderName,footageRef.width,footageRef.height,1,layerDuration,framerate);
                                curComp.parentFolder = projClipFolder;
                                var compStats=new Array(curComp,layerIN,layerOUT,layerBlendmode,layerOpacity,layerLabel);
                                myNewCompositions.push(compStats);
                                //Timing layers

                                for(k=0;k<footage.length;k++){
                                    curImage = footage[k];
                                    path = curImage.file.absoluteURI;
                                    imageNUM =  filepath_getNumber(path)*1;

                                    //frame duration
                                    var imageIN = imageNUM/framerate-layerIN/framerate;

                                    if(k==footage.length-1){
                                        var imageDuration =  curComp.duration-imageIN;
                                    }else{
                                        nextImage = footage[k+1];
                                        path = nextImage.file.absoluteURI;
                                        nextNUM =  filepath_getNumber(path)*1;
                                        var deltaFrame = nextNUM-imageNUM;
                                        var imageDuration = deltaFrame/framerate;
                                    }

                                    //finding file

                                    try{
                                        mylayers = curComp.layers.add(curImage);
                                        mylayers.inPoint = imageIN;
                                        mylayers.outPoint = imageIN+imageDuration;
                                    } catch(e){
                                        alert("Cant Add Frame "+xsht_file+"\nof "+nameMain+"\n Probably because the footage wasnt imported because there is a write failue in the File");
                                    }

                                }

                              


                            
                        }
                        
					//collect Layers In animation Comp

					var collectComp = app.project.items.addComp(clipFolder.displayName,myNewCompositions[0][0].width,myNewCompositions[0][0].height,1,clipduration/framerate,framerate);
					collectComp.parentFolder = projClipFolder;
				
				
					collectComp.bgColor =bgcolor;
					for(var p = myNewCompositions.length;p>0 ;p--){
						var curCompStats = myNewCompositions[p-1];
						var curComp=curCompStats[0];
						var curIN=curCompStats[1];
						var curOUT=curCompStats[2];
						var curBLEND=curCompStats[3];
						var curOPACITY=curCompStats[4];
						var curLABEL=curCompStats[5];
						var curLayer = collectComp.layers.add(curComp);
						layer_setBlendMode(curLayer,curBLEND);
						curLayer.startTime =(curIN-1)/framerate;
						curLayer.opacity.setValue(curOPACITY);
				
						 try{
								curLayer.label=curLABEL*1;
						} catch(e){
								curLayer.label=15;
						}

					
					}
                            
                    } else {
                        alert("'clipinfo.txt' do not exists in clipfolder. Please re-export from TVPaint");
                    }

            } else {
                alert("MESSAGE 2 :Please Select a PNG Exported From TVPaint" );
            }
        } catch (e) {
            alert("MESSAGE 3 :Please Select a PNG Exported From TVPaint "+e.line+":"+e.message);
        }
    }


} else {
	alert ("This Script requires the scripting security preference to be set.\nGo to the \"General\" panel of your application preferences, and make sure that \"Allow Scripts to Write Files and Access Network\" is checked.", "MJ TVPAINT IMPORT");
}



function folders_get(theFolder){
	try{
		//var returnFolders = new Array();
		//var j = 0;
		
		var folderItems = theFolder.getFiles(folders_find);
		return folderItems;
	}catch(e){
		alert(e.line+":"+e.message);
	}
}


function folders_find(theFolder){
	try{
		if(theFolder instanceof Folder)
		{
			return true;
		}else{
			return false;
		}
	}catch(e){
		alert(e.line+":"+e.message);
	}
}

function filepath_getNumber(path){
    var string_noExt = path.substring(0,path.length-4);
	var numberi = 0;
	var namei = 0;
	for(var i = string_noExt.length-1;i>=0;i--){
		if(isNaN(parseInt(string_noExt.charAt(i)))){
			numberi = i+1;
			break;
		}
		
	}
    number = string_noExt.substring(numberi,string_noExt.length);
    return number;
}


function layer_setBlendMode(curLayer,curBLEND){
						switch (curBLEND)
						{
						case "Color":
							curLayer.blendingMode = BlendingMode.NORMAL;
						  break;
						case "Add":
						 curLayer.blendingMode =  BlendingMode.ADD;
						case "Screen":
						  curLayer.blendingMode =  BlendingMode.SCREEN;
						  break;
						case "Multiply":
						    curLayer.blendingMode =  BlendingMode.MULTIPLY;
						  break;
						case "Overlay":
						  curLayer.blendingMode =  BlendingMode.OVERLAY;
						  break;
		
						  default:
						  curLayer.blendingMode = BlendingMode.NORMAL;
						}
						
						
}

function isSecurityPrefSet(){
	var securitySetting = app.preferences.getPrefAsLong("Main Pref Section", "Pref_SCRIPTING_FILE_NETWORK_SECURITY");
	return (securitySetting == 1);
}

		

		



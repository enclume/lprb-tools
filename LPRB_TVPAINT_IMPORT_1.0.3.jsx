﻿/**
 * LPRB_TVPAINT_IMPORT_1.0.3 (05-2023)
 * Import TVPaint script, based on the MJ_TVPAINT_IMPORT_1.0.3
 * dev: Guillaume Geelen 
 */

app.beginUndoGroup("TVPaint LPRM Import")
main()
app.endUndoGroup()


/**
 * 
 * @param {String} sq - Sxx
 * @param {String} pl - Pxx
 */
function get_tvpExportFolder(sq, pl) {
    //var tvpExport = "B:\\PRODUCTION\\COMPO\\01_CLEAN TO COMPO\\S01\\S01_P03\\PRB_CLEAN_S01_P03_A_V01"
    var folder = get_serverItem(["PRODUCTION", "COMPO", "01_CLEAN TO COMPO", sq, sq+"_"+pl]);

    /**
     * TODO
     * scan the folder for all relevant subFolders (with good nomenclature, last version, and with a clipinfo.txt inside)
     */
    return folder;
}

function main() {
    var activeItem = app.project.activeItem;
    var CompTree = import_TVpaint_imgs(activeItem);
    if (CompTree === undefined) {return;}
    order_compTree(CompTree)
}

function order_compTree(CompTree) {
    var topFolder = get_topFolder(); 
    var sqPl = get_sqPl(CompTree.projectFolder.name);

    CompTree.projectFolder.name = sqPl + "_AN";
    CompTree.mstrComp.name = sqPl + "_AN";

    var plFolder = get_folder(sqPl, topFolder);
    CompTree.projectFolder.parentFolder = plFolder;

    var plComp = get_comp(sqPl, plFolder);

    var txtr = get_compTexture(topFolder);
    //var tvpExport = "B:\\PRODUCTION\\COMPO\\01_CLEAN TO COMPO\\S01\\S01_P03\\PRB_CLEAN_S01_P03_A_V01"

    for (var i = CompTree.subComp.length; i > 0; i--) {
        var subComp = CompTree.subComp[i-1];
        var layer = plComp.layers.add(subComp.preComp);
        layer.label = 1 // change the color in ui

        if (layer.name.indexOf("_TR_") != -1) {
            layer.blendingMode = BlendingMode.MULTIPLY;
            var solid = subComp.preComp.layers.addSolid([1.0, 1.0, 1.0], "Solid Blanc", subComp.footage[0].width, subComp.footage[0].height, 1.0);
            solid.moveToEnd();
            
            if (app.isoLanguage == "fr_FR") {
                var fx = add_effect(layer, "Masquage linéaire par couleur");
                fx.property("Couleur de découpe").setValue([1.0, 1.0, 1.0]);
                fx.property("Lissage").setValue(10);
                fx = add_effect(layer, "Teinte/Saturation");
                fx.property("Redéfinir").setValue(true);
            } else if (app.isoLanguage == "en_US") {
                var fx = add_effect(layer, "Linear Color Key");
                fx.property("Key Color").setValue([1.0, 1.0, 1.0]);
                fx.property("Matching Softness").setValue(10);
                fx = add_effect(layer, "Hue/Saturation");
                fx.property("Colorize").setValue(true);
            } else {alert("Error : language is not En or Fr");}

        } else if (layer.name.indexOf("_OM_") != -1) {
            layer.blendingMode = BlendingMode.MULTIPLY;
            layer.opacity.setValue(75)
            add_txtr(plComp, txtr, layer, 10)
        } else if (layer.name.indexOf("_CL_") != -1) {
            layer.blendingMode = BlendingMode.NORMAL;
            add_txtr(plComp, txtr, layer, 30)
        }
    }
}


function add_effect(layer, fxName) {
    fxTrad = {"En": "Fr", "Linear Color Key": "Masquage linéaire par couleur", "Hue/Saturation": "Teinte/Saturation"}

    if (app.isoLanguage == "fr_FR") {
        var effects = layer.property("Effets");
    } else if (app.isoLanguage == "en_US") {
        var effects = layer.property("Effects");
    } else {
        alert("Error : language is not En or Fr");
        return;
    }

    effects.addProperty(fxName);
    return effects.property(fxName);
}


function get_sqPl(name) {
    var sq = "";
    var pl = "";

    name = name.split("_")
    for (var i = 0; i < name.length; i++) {
        var item = name[i];
        if (item[0]=="S" && item.length == 3) {
            sq = item
        }
        if (item[0]=="P" && item.length == 3) {
            pl = item
        }
    }
    return sq+"_"+pl
}


function import_TVpaint_imgs(item) {
    if (isSecurityPrefSet() == false) {
        var msg = "This Script requires the scripting security preference to be set."
        msg += "\nGo to the \"General\" panel of your application preferences, and make sure that \"Allow Scripts to Write Files and Access Network\" is checked."
        alert(msg, "LPRB TVPAINT IMPORT (1.0.3)")
        return;
    }
    
    var activeItem = item;
    if (activeItem === undefined) {activeItem = app.project.activeItem;}
    // if (activeItem === null) {
    //     alert("MESSAGE 1 : Please Select a PNG Exported From TVPaint" );
    //     return;
    // }

    try {
        // if (!is_footage(activeItem)) {
        //     alert("MESSAGE 2 : Please Select a PNG Exported From TVPaint (active item is not a footage, but is "+activeItem.typeName+")");
        //     return;
        // }

        var clip = get_clip_info(activeItem);
        if (clip == false) {
            return;
        }        
        
        // add a new folder in the project items
        var projClipFolder = create_folder(clip.folder.displayName);
        var CompTree = {"mstrComp": undefined, "subComp": [], "clip": clip, "projectFolder": projClipFolder};

        // for each clip's subFolders
        for(var i = 0; i < clip.subFolders.length; i++) {
            var layerFolder = clip.subFolders[i];
            var prcInfo = import_imgSeq_as_preComp(layerFolder.displayName, clip, layerFolder, projClipFolder)

            CompTree.subComp.push(prcInfo);
        }
            
        //create a global comp
        var collectComp = create_composition(clip.folder.displayName, CompTree.subComp[0].footage[0].width, CompTree.subComp[0].footage[0].height, 1, clip.duration/clip.framerate, clip.framerate, projClipFolder);
        collectComp.bgColor = clip.bgcolor;
        CompTree.mstrComp = collectComp;
        
        //add all subComp in the global comp
        for(var p = CompTree.subComp.length; p > 0; p--) {
            var prcInfo = CompTree.subComp[p-1];
            var curLayer = collectComp.layers.add(prcInfo.preComp);
            layer_setBlendMode(curLayer, prcInfo.layerInfo.Blendmode);
            curLayer.startTime = (prcInfo.layerInfo.IN - 1) / clip.framerate;
            curLayer.opacity.setValue(prcInfo.layerInfo.Opacity);
    
            try {
                curLayer.label=prcInfo.layerInfo.Label * 1;
            } catch(e) {
                curLayer.label=15;
            }
        }
        return CompTree;
    } catch (e) {
        alert("ERROR on script execution\n"+e.line+" : "+e.message);
    }
}


function create_composition(name, width, height, pixelAspect, duration, frameRate, parentFolder) {
    var newComp = app.project.items.addComp(name, width, height, pixelAspect, duration, frameRate);
    if (parentFolder) {newComp.parentFolder = parentFolder;}
    return newComp;
}

function create_folder(name, parentFolder) {
    var newFolder = app.project.items.addFolder(name);
    if (parentFolder) {newFolder.parentFolder = parentFolder;}
    return newFolder;
}

function get_compTexture(pFolder) {
    var txtrFolder = get_folder("TEXTURE", pFolder);
    if (txtrFolder == false) {
        txtrFolder = create_folder("TEXTURE", pFolder)
    }
    else {
        for (var i = 0; i < txtrFolder.items.length; i++) {
            var item = txtrFolder.items[i+1];
            if (!is_footage(item)) {continue;}
            if (item.file.absoluteURI.indexOf("LPRB_TXT_01_X1_0001.cr3") == -1) {continue;}
            return item
        }
    }
    var txtrFile = get_serverItem(["PRODUCTION", "DECORS", "COLO", "TEXTURE", "LRPB_TEXTURE_prisedevue", "LPRB_TXT.dgn", "LPRB_TXT_Take_01", "LPRB_TXT_01_X1", "LPRB_TXT_01_X1_0001.cr3"])
    return import_img(txtrFile.absoluteURI, txtrFolder)
}

function add_txtr(comp, txtr, layer, opacity) {
    if (opacity === undefined) {opacity = 30;}
    var txtrLayer = comp.layers.add(txtr);
    txtrLayer.label = 1
    txtrLayer.opacity.setValue(opacity);
    txtrLayer.moveBefore(layer);
    txtrLayer.setTrackMatte(layer, TrackMatteType.ALPHA);
    layer.enabled = true;
}

function import_imgSeq_as_preComp(name, clip, layerFolder, parentFolder) {
    var itemArray = layerFolder.getFiles("*.png");
    if (itemArray.length > 1) {itemArray.sort();}
    
    // create a project subFolder
    var projLayerFolder = create_folder(name, parentFolder);

    //import the pict in the clip subFolder
    var footage = import_img_sequence(itemArray, projLayerFolder);

    var layerInfo = get_layer_info(layerFolder.absoluteURI + "/layerinfo.txt", clip.duration);
    var layerDuration = (layerInfo.OUT - layerInfo.IN + 1) * (1/clip.framerate);
    
    //create the imgSeq container
    var preComp = create_composition(name, footage[0].width, footage[0].height, 1, layerDuration, clip.framerate, parentFolder);

    //add all pict in the composition
    for(var i = 0; i < footage.length; i++) {
        var img = footage[i];
        var imgNUM = filepath_getNumber(img.file.absoluteURI);
        var imgIN = (imgNUM - layerInfo.IN) / clip.framerate;

        //frame duration
        var imgDuration = preComp.duration-imgIN;
        if (i != footage.length-1) {
            nextNUM = filepath_getNumber(footage[i+1].file.absoluteURI);
            imgDuration = (nextNUM-imgNUM)/clip.framerate;
        }

        //add the layer
        try {
            var mylayers = preComp.layers.add(img);
            mylayers.inPoint = imgIN;
            mylayers.outPoint = imgIN+imgDuration;
        } catch(e) {
            var msg = "Can't Add Frame "+xsht_file+"\nof "+nameMain+"\n";
            msg += "Probably because the footage wasnt imported because there is a write failue in the File";
            alert(msg);
        }
    }
    return {"preComp": preComp, "footage": footage, "layerInfo": layerInfo, "projLayerFolder": projLayerFolder};
}

/**
 * @param {String[]} imgPaths - an array of image path
 * @param {*} parentFolder - a potential parent folder
 * @returns 
 */
function import_img_sequence(imgPaths, parentFolder) {
    if (parentFolder === undefined) {
        parentFolder == create_folder("unnamed import")
    }
    var footage = new Array();
    for(var i = 0; i < imgPaths.length; i++) {
        footage[i] = import_img(imgPaths[i], parentFolder);
    }

    return footage;
}


function import_img(imgPath, parentFolder) {
    var imgFile = new File(imgPath);
    var ioRef = new ImportOptions(imgFile);
    ioRef.importAs = ImportAsType.FOOTAGE;

    var footageRef = app.project.importFile(ioRef);
    if (parentFolder && is_folder(parentFolder)) {
        footageRef.parentFolder = parentFolder;
    }
    return footageRef;
}


function get_clip_info(element) {
    if (is_footage(element)) {
        var clipFolder = element.file.parent.parent;
        var clipInfoPath = clipFolder.absoluteURI+"/clipinfo.txt";
    } else if (true) {
        alert("No TVpaint png selected, please choose one")
        element = File.openDialog("choose the TVpaint exported file")
        if (element.absoluteURI.indexOf("clipinfo.txt") == -1) {
            var clipFolder = element.parent.parent;
            var clipInfoPath = clipFolder.absoluteURI+"/clipinfo.txt";
        } else {
            var clipFolder = element.parent;
            var clipInfoPath = element.absoluteURI;
        }
    }
    var clipInfoFile = new File(clipInfoPath);

    if (!clipInfoFile.exists) {
        alert("'clipinfo.txt' do not exists in clipfolder. Please re-export from TVPaint");
        return false;
    }

    clipInfoFile.open();

    var framerate = clipInfoFile.readln();
    framerate = framerate.split(";");
    framerate = framerate[1];

    var clipduration = clipInfoFile.readln();
    clipduration = clipduration.split(";");
    clipduration = clipduration[1];
    
    var bgcolor = clipInfoFile.readln();
    bgcolor = bgcolor.split(";");
    bgcolor = bgcolor[1];
    bgcolor = bgcolor.split(",");
    bgcolor = new Array(bgcolor[0],bgcolor[1],bgcolor[2]);

    clipInfoFile.close();

    var subFolders = clipFolder.getFiles(isFolder);

    return {"folder": clipFolder, "subFolders": subFolders, "framerate": framerate, "duration": clipduration, "bgcolor": bgcolor}
}


function get_layer_info(layerInfopath, clipduration) {
    var infoFile = new File(layerInfopath);

    if (!infoFile.exists) {
        var layerIN = 1;
        var layerOUT = layerIN + clipduration;
        var layerBlendmode = "COLOR";
        var layerOpacity = 100;
        var layerLabel = 0;
        return {"IN": layerIN, "OUT": layerOUT, "Blendmode": layerBlendmode, "Opacity": layerOpacity, "Label": layerLabel};
    }
    
    infoFile.open();

    var layerIN = infoFile.readln();
    layerIN = layerIN.split(";");
    layerIN = layerIN[1];
    
    var layerOUT = infoFile.readln();
    layerOUT = layerOUT.split(";");
    layerOUT = layerOUT[1];
    
    var layerBlendmode = infoFile.readln();
    layerBlendmode = layerBlendmode.split(";");
    layerBlendmode = layerBlendmode[1];
        
    var layerOpacity = infoFile.readln();
    layerOpacity = layerOpacity.split(";");
    layerOpacity = layerOpacity[1];

    try {							
        var layerLabel = infoFile.readln();
        layerLabel = layerLabel.split(";");
        layerLabel = layerLabel[1]*1;
    } catch(e) {
        layerLabel = 0;
    }

    infoFile.close();

    return {"IN": layerIN, "OUT": layerOUT, "Blendmode": layerBlendmode, "Opacity": layerOpacity, "Label": layerLabel};
}


/**
 * Filter methode to get only Folder type in Folder.getFiles()
 */
function isFolder(theFolder) {
    return theFolder instanceof Folder;
}

function filepath_getNumber(path) {
    var string_noExt = path.substring(0, path.length-4);
	var numberi = 0;
	for(var i = string_noExt.length-1; i >= 0; i--) {
		if(isNaN(parseInt(string_noExt.charAt(i)))) {
			numberi = i+1;
			break;
		}
	}
    number = string_noExt.substring(numberi, string_noExt.length);
    return number * 1;
}

function layer_setBlendMode(curLayer, curBLEND) {
    switch (curBLEND) {
        case "Color":
            curLayer.blendingMode = BlendingMode.NORMAL;
            break;
        case "Add":
            curLayer.blendingMode = BlendingMode.ADD;
            break;
        case "Screen":
            curLayer.blendingMode = BlendingMode.SCREEN;
            break;
        case "Multiply":
            curLayer.blendingMode = BlendingMode.MULTIPLY;
            break;
        case "Overlay":
            curLayer.blendingMode = BlendingMode.OVERLAY;
            break;
        default:
            curLayer.blendingMode = BlendingMode.NORMAL;
    }				
}

function isSecurityPrefSet() {
	var securitySetting = app.preferences.getPrefAsLong("Main Pref Section", "Pref_SCRIPTING_FILE_NETWORK_SECURITY");
	return (securitySetting == 1);
}

		
/**
 * @param {String} value 
 * @param {Number} length 
 * @returns 
 */
function zfill(value, length) {
    while (value.length < length) {
        value = "0" + value;
    }
    return value
}


function include(array, value) {
    for (var i = 0; i < array.length; i++) {
        var item = array[i];
        if (item === value || item == value) {return true;}
    }
    return false;
}


function is_footage(item) {
    if (item === null || item === undefined) {return false;}
    test = ["Footage", "Footage", "Material de archivo", "Métrage", "Metraggio", "フッテージ", "푸티지", "Gravação", "Видеоряд", "素材"];
    return include(test, item.typeName);
}


function is_folder(item) {
    if (item === null || item === undefined) {return false;}
    test = ["Folder", "Ordner", "Carpeta", "Dossier", "Cartella", "フォルダー", "폴더", "Pasta", "Папка", "文件夹"];
    return include(test, item.typeName);
}


function is_composition(item) {
    if (item === null || item === undefined) {return false;}
    test = ["Composition", "Komposition", "Composición", "Composizione", "コンポジション", "컴포지션", "Composição", "Композиция", "合成"];
    return include(test, item.typeName);
}


function get_folder(name, parent) {
    for (var i = 0; i < parent.items.length; i++) {
        var item = parent.items[i+1];
        if (item.name == name && is_folder(item) && item.parentFolder === parent) {return item;}
    }    
    alert("can't get the "+name+" folder")
    return false;
}


function get_comp(name, parent) {
    for (var i = 0; i < parent.items.length; i++) {
        var item = parent.items[i+1];
        if (item.name == name && is_composition(item) && item.parentFolder === parent) {return item;}
    }    
    alert("can't get the "+name+" comp")
    return false;
}


function get_topFolder() {
    var testF = app.project.items.addFolder("topFolder");
    var topFolder = testF.parentFolder;
    testF.remove()
    return topFolder
}

/**
 * get the root folder of the project
 */
function get_projectRoot() {
    var curFile = app.project.file;
    while (!include(["PRODUCTION", "PRE-PRODUCTION"], curFile.name)) {
        curFile = curFile.parent;
    }
    return curFile.parent;
}


/**
 * get a folder on the LPRM server
 * @param {String[]} pathItems - an array of string path item
 */
function get_serverItem(pathItems, baseFolder) {
    if (baseFolder) {var serverItem = baseFolder;}
    else {var serverItem = get_projectRoot();}
    for (var i = 0; i < pathItems.length; i++) {
        var pathItem = pathItems[i];
        var found = false;
        var subItems = serverItem.getFiles();
        for (var j = 0; j < subItems.length; j++) {
            if (subItems[j].name == pathItem) {
                serverItem = subItems[j];
                found = true;
                break;
            }
        }
        if (!found) {
            alert("can't find item \""+pathItem+"\" on server");
            return false;
        }
    }
    return serverItem;
}